// Chakra imports
import {
    Flex,
    SimpleGrid,
    Spacer,
    Stat,
    StatLabel,
    Table,
    Td,
    Tr,
    useColorModeValue,
  } from "@chakra-ui/react";
  // assets
  import peopleImage from "assets/img/people-image.png";
  import logoChakra from "assets/svg/logo-white.svg";
  // Custom components
  import Card from "components/Card/Card.js";
  import CardBody from "components/Card/CardBody.js";
  import CardHeader from "components/Card/CardHeader.js";
  import BarChart from "components/Charts/BarChart";
  import LineChart from "components/Charts/LineChart";
  import IconBox from "components/Icons/IconBox";
  import YoutubeEmbed from "components/YoutubeEmbed/YoutubeEmbed";

  // Custom icons
  import {
    CartIcon,
    DocumentIcon,
    GlobeIcon,
    // RocketIcon,
    // StatsIcon,
    WalletIcon,
  } from "components/Icons/Icons.js";
//   import DashboardTableRow from "components/Tables/DashboardTableRow";
//   import TimelineRow from "components/Tables/TimelineRow";
  import React, { useState } from "react";
  // react icons
//   import { BsArrowRight } from "react-icons/bs";
//   import { IoCheckmarkDoneCircleSharp } from "react-icons/io5";
//   import { dashboardTableData, timelineData } from "variables/general";
  
  export default function Home(props) {
    const { data } = props;
    // Chakra Color Mode
    // const { colorMode, toggleColorMode } = useColorMode();
    const iconTeal = useColorModeValue("teal.300", "teal.300");
    const iconBoxInside = useColorModeValue("white", "white");
    const textColor = useColorModeValue("gray.700", "white");
    const [series, setSeries] = useState([
      {
        type: "area",
        name: "Mobile apps",
        data: [190, 220, 205, 350, 370, 450, 400, 360, 210, 250, 292, 150],
      },
      {
        type: "area",
        name: "Websites",
        data: [400, 291, 121, 117, 25, 133, 121, 211, 147, 25, 201, 203],
      },
    ]);
    const overlayRef = React.useRef();

    const _getTime = (_val) => {
        const x     = _val;
        const y     = x % 3600;
        const jam   = x / 3600;
        const menit = y / 60;
        // const detik = y % 60;
        
        return Math.floor(jam) + 'J ' + Math.floor(menit) + 'M';
    }

    const _getResolution = (_val) => {
        let id = 0;
        let name = '-';

        _val.forEach(element => {
            if(element?.master?.id > id){
                id = element?.master?.id;
                name = element?.master?.name + ' (' + element?.master?.pixel + 'p)';
            }
            // _data.push(element?.master?.name + '( ' + element?.master?.pixel + 'p )');
        });

        return name;
    }

    const _getOther = (_val) => {
        const _data = [];
        _val.forEach(element => {
            _data.push(element?.master?.name);
        });

        return _data.join(', ');
    }
  
    return (
      <Flex flexDirection="column" pt={{ base: "120px", md: "75px" }}>
        <SimpleGrid columns={{ sm: 1, md: 2, xl: 2 }} spacing="24px">
            {data.map((_item) => (
                <Card minH="83px">
                    <CardBody>
                    <Flex flexDirection="row" align="center" justify="center" w="100%">
                        <Stat me="auto">
                        <StatLabel
                            fontSize="m"
                            color="black.400"
                            fontWeight="bold"
                            pb=".1rem"
                        >
                            {_item?.title}
                        </StatLabel>
                        <YoutubeEmbed embedId={_item?.path_video} />
                        <StatLabel
                            fontSize="sm"
                            color="black.400"
                            fontWeight="bold"
                            mt=".5rem"
                        >
                            {`${_item?.publication_year} [${_item?.age_rating}+] ${_getTime(_item?.duration)}`}
                        </StatLabel>
                        <Spacer
                            mt=".5rem"
                        >
                            {_item?.description}
                        </Spacer>
                        <Table mt="10px" width="auto" variant="simple" color={textColor}>
                            <Tr ps="0px">
                                <Td p="5px 0px">Resolusi </Td>
                                <Td fontWeight="bold" p="5px 0px"> : { _getResolution(_item?.resolution) } </Td>
                            </Tr>
                            <Tr ps="0px">
                                <Td p="5px 0px">Sutradara </Td>
                                <Td fontWeight="bold" p="5px 0px"> : {_item?.director} </Td>
                            </Tr>
                            <Tr ps="0px">
                                <Td p="5px 0px">Pemeran </Td>
                                <Td fontWeight="bold" p="5px 0px"> : { _getOther(_item?.artist) }  </Td>
                            </Tr>
                            <Tr ps="0px">
                                <Td p="5px 0px">Genre </Td>
                                <Td fontWeight="bold" p="5px 0px"> : { _getOther(_item?.genre) }  </Td>
                            </Tr>
                            <Tr ps="0px">
                                <Td p="5px 0px">Kategori </Td>
                                <Td fontWeight="bold" p="5px 0px"> : { _getOther(_item?.category) }  </Td>
                            </Tr>
                            <Tr ps="0px">
                                <Td p="5px 0px">Rating Usia </Td>
                                <Td fontWeight="bold" p="5px 0px"> : { `[${_item?.age_rating}+] Direkomendasikan untuk ${_item?.age_rating} ke atas ` }  </Td>
                            </Tr>
                        </Table>
                        {/* <Flex>
                            <StatNumber fontSize="lg" color={textColor}>
                            $53,000
                            </StatNumber>
                            <StatHelpText
                            alignSelf="flex-end"
                            justifySelf="flex-end"
                            m="0px"
                            color="green.400"
                            fontWeight="bold"
                            ps="3px"
                            fontSize="md"
                            >
                            +55%
                            </StatHelpText>
                        </Flex> */}
                        </Stat>
                        {/* <IconBox as="box" h={"45px"} w={"45px"} bg={iconTeal}>
                        <WalletIcon h={"24px"} w={"24px"} color={iconBoxInside} />
                        </IconBox> */}
                    </Flex>
                    </CardBody>
                </Card>
            ))}
        </SimpleGrid>
      </Flex>
    );
  }
  