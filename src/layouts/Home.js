// Chakra imports
import { ChakraProvider, Portal, useDisclosure } from "@chakra-ui/react";
// import Configurator from "components/Configurator/Configurator";
// import Footer from "components/Footer/Footer.js";
// Layout components
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import React, { useState, useEffect } from "react";
import { useLocation, } from "react-router-dom";
// import routes from "routes.js";
// Custom Chakra theme
import theme from "theme/theme.js";
// import FixedPlugin from "../components/FixedPlugin/FixedPlugin";
// Custom components
import MainPanel from "../components/Layout/MainPanel";
import PanelContainer from "../components/Layout/PanelContainer";
import PanelContent from "../components/Layout/PanelContent";
import { API_ENDPOINTS } from "helpers/constants";
import Home from "views/Home/index.js";

export default function Dashboard(props) {
  const { ...rest } = props;
  let location = useLocation();

  // states and functions
  const [sidebarVariant, setSidebarVariant] = useState("transparent");
  const [fixed, setFixed] = useState(false);
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [menus, setMenus] = useState([]);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(API_ENDPOINTS.genre)
      .then(res => res.json())
      .then(
        (response) => {
          setIsLoaded(true);
          setMenus(response?.results);
        },
        (error) => {
          setMenus([]);
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [])

  useEffect(() => {
    const config = {
      method: 'GET',
      headers: {
        // ...DEFAULT_HEADERS,
        // ...headers,
      },
    };
    const _path = location?.pathname.replace(/\//g, "");
    const _search = location?.search.replace("?","");
    const _item = menus?.find((_data) => {
      if(_data?.slug === _path) return _data?.id;
    })
    let _url = `${API_ENDPOINTS.list}?`;

    if(_item){
      _url += `genre_id=${_item?.id}`;
    }

    if(_search){
      _url += `&${_search}`;
    }

    fetch(_url, config)
      .then(res => res.json())
      .then(
        (response) => {
          setIsLoaded(true);
          setData(response?.results);
        },
        (error) => {
          setData([]);
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [menus, location?.pathname, location?.search])

  // ref for main panel div
  const mainPanel = React.createRef();
  // functions for changing the states from components
  const getRoute = () => {
    return window.location.pathname !== "/admin/full-screen-maps";
  };
  const getActiveRoute = (routes) => {
    let activeRoute = "Default Brand Text";
    for (let i = 0; i < routes.length; i++) {
      if (routes[i].collapse) {
        let collapseActiveRoute = getActiveRoute(routes[i].views);
        if (collapseActiveRoute !== activeRoute) {
          return collapseActiveRoute;
        }
      } else if (routes[i].category) {
        let categoryActiveRoute = getActiveRoute(routes[i].views);
        if (categoryActiveRoute !== activeRoute) {
          return categoryActiveRoute;
        }
      } else {
        if (
          window.location.href.indexOf(routes[i].layout + routes[i].path) !== -1
        ) {
          return routes[i].name;
        }
      }
    }
    return activeRoute;
  };
  // This changes navbar state(fixed or not)
  const getActiveNavbar = (routes) => {
    let activeNavbar = false;
    for (let i = 0; i < routes.length; i++) {
      if (routes[i].category) {
        let categoryActiveNavbar = getActiveNavbar(routes[i].views);
        if (categoryActiveNavbar !== activeNavbar) {
          return categoryActiveNavbar;
        }
      } else {
        if (
          window.location.href.indexOf(routes[i].layout + routes[i].path) !== -1
        ) {
          if (routes[i].secondaryNavbar) {
            return routes[i].secondaryNavbar;
          }
        }
      }
    }
    return activeNavbar;
  };

  const { isOpen, onOpen, onClose } = useDisclosure();
  document.documentElement.dir = "ltr";
  // Chakra Color Mode
  return (
    <ChakraProvider theme={theme} resetCss={false}>
      <Sidebar
        routes={menus}
        logoText={"MOVIE THEATER"}
        display="none"
        sidebarVariant={sidebarVariant}
        {...rest}
      />
      <MainPanel
        ref={mainPanel}
        w={{
          base: "100%",
          xl: "calc(100% - 275px)",
        }}
      >
        <Portal>
          <AdminNavbar
            onOpen={onOpen}
            logoText={"MOVIE THEATER"}
            routes={menus}
            fixed={fixed}
            {...rest}
          />
        </Portal>
        {getRoute() ? (
          <PanelContent>
            <PanelContainer>
              <Home 
                data={data}
              />
              {/* <Switch>
                <Route
                  path={'/'}
                  component={Home}
                  key={0}
                />
              </Switch> */}
            </PanelContainer>
          </PanelContent>
        ) : null}
      </MainPanel>
    </ChakraProvider>
  );
}
