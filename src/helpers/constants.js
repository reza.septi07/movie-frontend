const API_HOST = 'https://api.rezaseptian.online/api/';

export const API_ENDPOINTS = {
    genre: API_HOST+'genre',
    list: API_HOST+'list',
};