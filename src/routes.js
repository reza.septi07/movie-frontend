// import

import Home from "views/Dashboard/Home.js";
import Tables from "views/Dashboard/Tables.js";
// import Billing from "views/Dashboard/Billing.js";
// import RTLPage from "views/RTL/RTLPage.js";

import {
  HomeIcon,
  StatsIcon,
  CreditIcon,
  PersonIcon,
  DocumentIcon,
  RocketIcon,
  SupportIcon,
} from "components/Icons/Icons";


const dashRoutes = () => {

  return [
    {
      path: "/dashboard",
      name: "Home",
      rtlName: "لوحة القيادة",
      icon: <HomeIcon color="inherit" />,
      component: Home,
      layout: "/admin",
    },
    {
      path: "/tables",
      name: "Tables",
      rtlName: "لوحة القيادة",
      icon: <StatsIcon color="inherit" />,
      component: Tables,
      layout: "/admin",
    },
  ];
};

export default dashRoutes();
